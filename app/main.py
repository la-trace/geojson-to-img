import logging
import os

from fastapi import FastAPI
from geojson_to_img import Render
from pydantic import BaseModel
from starlette.responses import Response

logger = logging.getLogger("api")
logger.setLevel(logging.DEBUG)


class GeoJSON(BaseModel):
    type: str
    coordinates: list


cache_path = os.getenv('GEORENDER_CACHE_PATH', "./cache")

app = FastAPI()


@app.post("/")
def read_root(geojson: dict, response: Response, w: int = 1024, h: int = 1024, stroke: int = 3):
    r = Render(geojson, cache_path, w, h, stroke, logger)
    i = r.process()
    return Response(i, media_type='image/png')
